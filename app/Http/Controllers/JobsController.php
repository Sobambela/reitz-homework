<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Http\Requests\StoreJobsRequest;

class JobsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $jobData = $request->all();
        $jobIds = [];
        if(isset($jobData['jobs']) && !empty($jobData['jobs'])) {

            foreach($jobData['jobs'] as $job){

                Redis::incr('job:id');

                $id = $this->getJobId();

                $jobData = [
                    'id' => $id,
                    'url' => $job['url'],
                    'html_selector' => $job['html_selector'],
                    'css_selector' => $job['css_selector'],
                    'status' => 'Pending',
                    'scraped_data' => ''
                ];

                Redis::set("job:$id", json_encode($jobData));
                $jobIds[] = $id;
            }

            $message = "Jobs Added successfully";
            $httpStatus = "200";
        } else {
            $message = "Job Added successfully";
            $httpStatus = "201";
        }
        
        return response()->json([
            'status' => $httpStatus,
            'message' => $message,
            'jobIds' => $jobIds
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $checkId = Redis::get("job:$id");
        $jobData = [];
        if($checkId) {

            $jobData = $checkId;

            $message = "Job found";
            $httpStatus = "200";
        } else {
            $message = "Job not found";
            $httpStatus = "404";
        }

        return response()->json([
            'status' => $httpStatus,
            'message' => $message,
            'job' => $jobData,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        $checkId = Redis::get("job:$id");
        $jobData = [];
        if($checkId) {

            Redis::delete("job:$id");
            // Redis::flushDB();

            $message = "Job deleted";
            $httpStatus = "200";
        } else {
            $message = "Job not found";
            $httpStatus = "404";
        }

        return response()->json([
            'status' => $httpStatus,
            'message' => $message
        ]);
    }

    public function getJobId(): int
    {
        $id = Redis::get('job:id');

        if (!$id) {
            Redis::incr('job:id');
            $id = Redis::get('job:id');
        }

        return $id;
    }
}
