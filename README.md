

## About Project

A Laravel-Based Backend for Frontend Integration with a set of RESTful
endpoints to meet the following criteria

## Set up

The project uses Docker containers with services for Laravel, MySQL and Redis. All storage is handled by Redis.

Clone the git repository below: 

$ git clone https://gitlab.com/Sobambela/reitz-homework.git

$ cd reitz-homework

$ composer install

$ ./vendor/bin/sail up

The last command will spin up the project containers and make it available at http://localhost:8085

## API Endoints

Important headers 

Accept: application/json
Content-Type: application/json

### Creating Jobs

POST: /api/jobs

Payload example:
{
    "jobs": [
        {
            "url": "http://localhost",
            "html_selector": "h2",
            "css_selector": ""
        },
        {
            "url": "http://localhost",
            "html_selector": "",
            "css_selector": ".leading-relaxed"
        }
    ]
}

Response Example:
{
    "status":"200",
    "message":"Jobs Added successfully",
    "jobIds":[
        35,
        36
    ]
}

### Getting a Job by ID

GET: /api/jobs/{id}

Response Example:
{
    "status":"200",
    "message":"Job found",
    "job":{
        "id":33,
        "url":"http:\/\/localhost",
        "html_selector":"h2",
        "css_selector":"",
        "status":"Complete",
        "scraped_data":"Documentation|Laracasts|Laravel News|Vibrant Ecosystem"
    }
}

### Deleting a Job by ID

GET: /api/jobs/{id}

Response Example:
{
    "status":"200",
    "message":"Job deleted"
}

## Scraping HTML and Running Jobs

After having added Jobs via the endpoint above. The following command will Queue them up and have them run asynchronously.

$ php artisan app:scrape-urls

To check for scraped data, use the GET endpoint ubove.
